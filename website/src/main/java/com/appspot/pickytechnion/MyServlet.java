/*
   For step-by-step instructions on connecting your Android application to this backend module,
   see "App Engine Java Servlet Module" template documentation at
   https://github.com/GoogleCloudPlatform/gradle-appengine-templates/tree/master/HelloWorld
*/

package com.appspot.pickytechnion;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.TimeZone;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MyServlet extends HttpServlet {
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {
        resp.setContentType("text/plain");
        resp.getWriter().println("Please use the form to POST to this url");
    }
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {
        String name = req.getParameter("name");
        String email = req.getParameter("email");
        String content = req.getParameter("content");
        resp.setContentType("text/plain");

        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdfDate.setTimeZone(TimeZone.getTimeZone("Israel"));
        Properties props = new Properties();
        Session session = Session.getDefaultInstance(props, null);

        try {
            Message msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress("pickytechnion@gmail.com", "picky website"));
            msg.addRecipient(Message.RecipientType.TO,
                    new InternetAddress("pickytechnion@gmail.com", "Picky Admins"));
            msg.setSubject("Message from picky website - " + sdfDate.format(new Date()));
            msg.setText("Message from: " + name + ", " + email + "\n" + content);
            Transport.send(msg);
        } catch (Exception e) {
            resp.getWriter().println(String.format("Error. Please send us mail directly"));
        }
        resp.getWriter().println(String.format("Message sent. Thank you"));
    }
}
